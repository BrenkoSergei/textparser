package by.bsuir.textparser.service;

import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.TextGroup;

import java.util.List;

public interface TextService {

    TextGroup findSimilarText(Text text, List<Text> textList, int minOverlaps);

    List<TextGroup> findSimilarText(List<Text> textList, int minOverlap);

    List<String> summarize(Text text, float percent);
}
