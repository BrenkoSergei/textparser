package by.bsuir.textparser.service;

import by.bsuir.textparser.Word;
import by.bsuir.textparser.entity.Sentence;
import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.TextGroup;
import by.bsuir.textparser.entity.TextStatistic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TextServiceImpl implements TextService {

    @Override
    public TextGroup findSimilarText(Text text, List<Text> textList, int minOverlaps) {
        TextGroup textGroup = new TextGroup(text, text.getName());
        for (Text textItem : textList) {
            List<Word> overlaps = getOverlaps(text.getTextStatistic(), textItem.getTextStatistic());
            if (overlaps.size() >= minOverlaps) {
                textGroup.addSimilarText(textItem);
            }
        }
        return textGroup;
    }

    @Override
    public List<TextGroup> findSimilarText(List<Text> textList, int minOverlap) {
        List<TextGroup> textGroupList = new ArrayList<>();
        for (int i = 0; i < textList.size(); i++) {
            Text text1 = textList.get(i);
            TextGroup textGroup = new TextGroup(text1, text1.getName());
            for (int j = i + 1; j < textList.size(); j++) {
                List<Word> overlaps = getOverlaps(text1.getTextStatistic(), textList.get(j).getTextStatistic());
                if(overlaps.size() >= minOverlap) {
                    textGroup.addSimilarText(textList.get(j));
                }
            }
            textGroupList.add(textGroup);
        }
        return textGroupList;
    }

    private List<Word> getOverlaps(TextStatistic text1, TextStatistic text2) {
        List<Word> wordOverlap = new ArrayList<>();
        List<Word> text1KeyWords = text1.getKeyWords();
        List<Word> text2KeyWords = text2.getKeyWords();
        for(Word word : text1KeyWords) {
            if(text2KeyWords.contains(word)) {
                wordOverlap.add(word);
            }
        }
        return wordOverlap;
    }

    @Override
    public List<String> summarize(Text text, float percent) {
        TextStatistic ts = text.getTextStatistic();
        List<Word> keyWords = ts.getKeyWords();
        List<Word> originalWordsList = new ArrayList<>();

        for(Word word : keyWords) {
            originalWordsList.addAll(ts.getOriginalWordList(word));
        }

        List<Sentence> sentenceList = originalWordsList.stream().map(Word::getSentence).collect(Collectors.toList());


        int wordCount = sentenceList.stream().mapToInt(sentence -> sentence.getWordList().size()).sum();
        int borderSize = (int) (text.getTextStatistic().getWordCount() * percent);
        int avgOnSentence = text.getTextStatistic().getAverageWordCountPerSentence();

        int diff = (wordCount - borderSize) / avgOnSentence;

        if(diff > 0) {
            int step = sentenceList.size() / diff;
            for(int i = 0; i < diff; i++) {
                sentenceList.remove(step * i - i);
            }
        }

        sentenceList.sort(Comparator.comparingInt(Sentence::getIndex));
        return sentenceList.stream().map(Sentence::getOriginalSentence).collect(Collectors.toList());
    }

}
