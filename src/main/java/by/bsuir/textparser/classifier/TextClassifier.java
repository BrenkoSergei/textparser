package by.bsuir.textparser.classifier;

import static org.encog.persist.EncogDirectoryPersistence.loadObject;
import static org.encog.persist.EncogDirectoryPersistence.saveObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.textparser.classifier.model.Characteristic;
import by.bsuir.textparser.classifier.model.CharacteristicValue;
import by.bsuir.textparser.classifier.model.ClassifiableText;
import by.bsuir.textparser.classifier.model.VocabularyWord;
import by.bsuir.textparser.classifier.ngram.NGramStrategy;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.Propagation;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.manhattan.ManhattanPropagation;
import org.encog.neural.networks.training.propagation.quick.QuickPropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.networks.training.propagation.scg.ScaledConjugateGradient;
import org.encog.persist.PersistError;

public class TextClassifier {

    private final Characteristic characteristic;
    private final int inputLayerSize;
    private final int outputLayerSize;
    private final BasicNetwork network;
    private final List<VocabularyWord> vocabulary;
    private final NGramStrategy nGramStrategy;
//    private final List<Observer> observers = new ArrayList<>();

    public TextClassifier(File trainedNetwork, Characteristic characteristic, List<VocabularyWord> vocabulary, NGramStrategy nGramStrategy) {
        if (characteristic == null ||
                characteristic.getName().equals("") ||
                characteristic.getPossibleValues() == null ||
                characteristic.getPossibleValues().size() == 0 ||
                vocabulary == null ||
                vocabulary.size() == 0 ||
                nGramStrategy == null) {
            throw new IllegalArgumentException();
        }

        this.characteristic = characteristic;
        this.vocabulary = vocabulary;
        this.inputLayerSize = vocabulary.size();
        this.outputLayerSize = characteristic.getPossibleValues().size();
        this.nGramStrategy = nGramStrategy;

        if (trainedNetwork == null) {
            this.network = createNeuralNetwork();
        } else {
            // load neural network from file
            try {
                this.network = (BasicNetwork) loadObject(trainedNetwork);
            } catch (PersistError e) {
                throw new IllegalArgumentException();
            }
        }
    }

    public TextClassifier(Characteristic characteristic, List<VocabularyWord> vocabulary, NGramStrategy nGramStrategy) {
        this(null, characteristic, vocabulary, nGramStrategy);
    }

    public static void shutdown() {
        Encog.getInstance().shutdown();
    }

    private BasicNetwork createNeuralNetwork() {
        BasicNetwork network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, inputLayerSize));

        /*
                первые 2 - для полного текста, вторые - по ключевым словам
         */
//        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputLayerSize / 6));
//        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputLayerSize / 6 / 3));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, (int) (inputLayerSize * 3.75)));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, (int) (inputLayerSize * 2.25)));
//        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, (int) (inputLayerSize * 2)));
//        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, (int) (inputLayerSize * 1.25)));

        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, outputLayerSize));
        network.getStructure().finalizeStructure();
        network.reset();

        return network;
    }

    public void train(List<ClassifiableText> classifiableTexts) {
        double[][] input = getInput(classifiableTexts);
        double[][] ideal = getIdeal(classifiableTexts);

        Propagation train = new Backpropagation(network, new BasicMLDataSet(input, ideal));
        train.setThreadCount(16);

        do {
            train.iteration();
            System.out.println("Training error: " + train.getError());
        } while (train.getError() > 0.000005);

        train.finishTraining();
    }

    public CharacteristicValue classify(ClassifiableText classifiableText) {
        double[] output = new double[outputLayerSize];

        // calculate output vector
        network.compute(getTextAsVectorOfWords(classifiableText), output);
        Encog.getInstance().shutdown();

        return convertVectorToCharacteristic(output);
    }

    public Map<String, Double> classify(String text) {
        double[] output = new double[outputLayerSize];

        // calculate output vector
        network.compute(getTextAsVectorOfWords(new ClassifiableText(text)), output);
        Encog.getInstance().shutdown();

        Map<String, Double> categoryToProbability = new HashMap<>(output.length);
        Map<Integer, String> idToCategoryName = characteristic.getPossibleValues().stream()
                .collect(Collectors.toMap(CharacteristicValue::getOrderNumber, CharacteristicValue::getValue));
        for (int i = 0; i < output.length; i++) {
            categoryToProbability.put(idToCategoryName.get(i), output[i]);
        }
        return categoryToProbability;
    }

    private CharacteristicValue convertVectorToCharacteristic(double[] vector) {
        int idOfMaxValue = getIdOfMaxValue(vector);

        // find CharacteristicValue with found Id
        //

        for (CharacteristicValue c : characteristic.getPossibleValues()) {
            if (c.getOrderNumber() == idOfMaxValue) {
                return c;
            }
        }

        return null;
    }

    private int getIdOfMaxValue(double[] vector) {
        int indexOfMaxValue = 0;
        double maxValue = vector[0];

        for (int i = 1; i < vector.length; i++) {
            if (vector[i] > maxValue) {
                maxValue = vector[i];
                indexOfMaxValue = i;
            }
        }

        return indexOfMaxValue + 1;
    }

    public void saveTrainedClassifier(File trainedNetwork) {
        saveObject(trainedNetwork, network);
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    private double[][] getInput(List<ClassifiableText> classifiableTexts) {
        double[][] input = new double[classifiableTexts.size()][inputLayerSize];

        // convert all classifiable texts to vectors
        //

        int i = 0;

        for (ClassifiableText classifiableText : classifiableTexts) {
            input[i++] = getTextAsVectorOfWords(classifiableText);
        }

        return input;
    }

    private double[][] getIdeal(List<ClassifiableText> classifiableTexts) {
        double[][] ideal = new double[classifiableTexts.size()][outputLayerSize];

        // convert all classifiable text characteristics to vectors
        //

        int i = 0;

        for (ClassifiableText classifiableText : classifiableTexts) {
            ideal[i++] = getCharacteristicAsVector(classifiableText);
        }

        return ideal;
    }

    // example:
    // count = 5; id = 4;
    // vector = {0, 0, 0, 1, 0}
    private double[] getCharacteristicAsVector(ClassifiableText classifiableText) {
        double[] vector = new double[outputLayerSize];
        Set<CharacteristicValue> characteristicValues = classifiableText.getCharacteristicValue(characteristic.getName());
        for (CharacteristicValue characteristicValue : characteristicValues) {
            vector[characteristicValue.getOrderNumber()] = 1;
        }
        return vector;
    }

    private double[] getTextAsVectorOfWords(ClassifiableText classifiableText) {
        double[] vector = new double[inputLayerSize];

        // convert text to nGramStrategy
        Set<String> uniqueValues = nGramStrategy.getNGram(classifiableText.getText());

        // create vector
        //

        for (String word : uniqueValues) {
            VocabularyWord vw = findWordInVocabulary(word);

            if (vw != null) { // word found in vocabulary
                vector[vw.getId()] = 1;
            }
        }

        return vector;
    }

    private VocabularyWord findWordInVocabulary(String word) {
        try {
            return vocabulary.get(vocabulary.indexOf(new VocabularyWord(word)));
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return characteristic.getName() + "NeuralNetworkClassifier";
    }
}