package by.bsuir.textparser.classifier.ngram;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.textparser.algorithm.porter.PorterStemmer;
import by.bsuir.textparser.classifier.WordCharacteristic;

public class FilteredUnigram implements NGramStrategy {

    @Override
    public Set<String> getNGram(String text) {
        // get all significant words
        String[] words = clean(text).split("[ \n\t\r$+<>№=]");

        // remove endings of words
        for (int i = 0; i < words.length; i++) {
            words[i] = PorterStemmer.stem(words[i]);
        }

        Set<String> uniqueValues = new LinkedHashSet<>(Arrays.asList(words));
        uniqueValues.removeIf(s -> s.equals(""));

        return uniqueValues;
    }

    @Override
    public Set<WordCharacteristic> getNGram(Set<WordCharacteristic> wordCharacteristics) {
        return wordCharacteristics.stream()
                .map(wordCharacteristic -> {
                    String stemmedWord = PorterStemmer.stem(wordCharacteristic.getWord());
                    return new WordCharacteristic(stemmedWord, wordCharacteristic.getOccurrenceFrequency());
                })
                .filter(wordCharacteristic -> !"".equals(wordCharacteristic.getWord()))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private String clean(String text) {
        // remove all digits and punctuation marks
        if (text != null) {
            return text.toLowerCase().replaceAll("[\\pP\\d]", " ");
        } else {
            return "";
        }
    }
}