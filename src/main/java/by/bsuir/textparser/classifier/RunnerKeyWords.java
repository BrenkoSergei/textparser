package by.bsuir.textparser.classifier;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import by.bsuir.textparser.Word;
import by.bsuir.textparser.classifier.model.Characteristic;
import by.bsuir.textparser.classifier.model.CharacteristicValue;
import by.bsuir.textparser.classifier.model.ClassifiableText;
import by.bsuir.textparser.classifier.model.VocabularyWord;
import by.bsuir.textparser.classifier.ngram.FilteredUnigram;
import by.bsuir.textparser.classifier.ngram.NGramStrategy;
import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.TextStatistic;
import by.bsuir.textparser.utill.TextParser;
import org.apache.commons.lang3.StringUtils;

public class RunnerKeyWords {

    private static final String TEXTS_FOLDER = "text";
    private static final String CATEGORIES_FOLDER = "category";

    private static final String REAL_DATA_FOLDER = "check";
    
    private static final int KEY_WORDS_COUNT = 6;

    public static void main(String[] args) throws URISyntaxException, IOException {
        TextClassifier classifier = train();

        URL resource = RunnerKeyWords.class.getClassLoader().getResource(REAL_DATA_FOLDER);
        Map<String, String> texts = loadFiles(resource);
        texts.forEach((file, text) -> {
            Text parsedText = TextParser.buildText(file, text);
            TextStatistic textStatistic = parsedText.getTextStatistic();

            String keyWords = textStatistic.getKeyWords(0, KEY_WORDS_COUNT).stream()
                    .map(Word::getWord)
                    .reduce((s1, s2) -> s1 + " " + s2 + " ").orElse("");
            Map<String, Double> classification = classifier.classify(keyWords);

            System.out.println("=======" + file + "=======");
            System.out.println(keyWords);
            classification.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder()))
                    .limit(10)
                    .map(categoryToProbability -> String.format("%s -> %1.6f", categoryToProbability.getKey(), categoryToProbability.getValue()))
                    .forEach(System.out::println);
            System.out.println();
        });
    }

    private static TextClassifier train() throws IOException, URISyntaxException {
        URL textsFolder = RunnerKeyWords.class.getClassLoader().getResource(TEXTS_FOLDER);
        URL categoriesFolder = RunnerKeyWords.class.getClassLoader().getResource(CATEGORIES_FOLDER);
        Map<String, String> fileToText = loadFiles(textsFolder);
        Map<String, Set<String>> fileToCategory = loadFilesLines(categoriesFolder);

        Map<String, List<String>> fileToKeyWords = getFileToKeyWordsList(fileToText);
        Map<String, String> fileToKeyWordsString = fileToKeyWords.entrySet().stream()
                .peek(printTrainingDataInfo(fileToCategory))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().stream()
                                .reduce((word1, word2) -> word1 + " " + word2 + " ")
                                .orElse(""))
                );


        NGramStrategy nGramStrategy = new FilteredUnigram();
        List<VocabularyWord> vocabularyWords = getVocabulary(fileToKeyWordsString, nGramStrategy);
        Set<CharacteristicValue> categories = getCategories(fileToCategory);

        Characteristic characteristic = new Characteristic("Category", categories);
        List<ClassifiableText> classifiableTexts = getClassifiableTexts(fileToKeyWordsString, fileToCategory, categories, characteristic);

        TextClassifier classifier = new TextClassifier(characteristic, vocabularyWords, nGramStrategy);
        classifier.train(classifiableTexts);
        return classifier;
    }

    private static Map<String, List<String>> getFileToKeyWordsList(Map<String, String> fileToText) {
        return fileToText.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> TextParser.buildText(entry.getKey(), entry.getValue())
                                .getTextStatistic()
                                .getKeyWords(0, KEY_WORDS_COUNT).stream()
                                .map(Word::getWord)
                                .collect(Collectors.toList()))
                );
    }

    private static Consumer<Map.Entry<String, List<String>>> printTrainingDataInfo(Map<String, Set<String>> fileToCategory) {
        return stringListEntry -> {
            Set<String> categories = fileToCategory.get(stringListEntry.getKey());
            List<String> keyWords = stringListEntry.getValue();

            StringBuilder categoriesString = new StringBuilder();
            categories.forEach(s -> categoriesString.append(s).append(", "));
            categoriesString.setLength(categoriesString.length() - 2);
            System.out.printf("------------ %s (%s) ------------\n", stringListEntry.getKey(), categoriesString);

            Iterator<String> keyWordsIterator = keyWords.iterator();
            double wordsInRow = 8;
            for (int i = 0; i < Math.ceil(keyWords.size() / wordsInRow); i++) {
                for (int j = 0; j < wordsInRow && i * wordsInRow + j < keyWords.size(); j++) {
                    System.out.printf("%15s", keyWordsIterator.next());
                }
                System.out.println();
            }
        };
    }

    private static Map<String, String> loadFiles(URL folder) throws IOException, URISyntaxException {
        return Files.list(Paths.get(folder.toURI()))
                .filter(Files::isReadable)
                .collect(Collectors.toMap(
                        path -> path.getFileName().toString(),
                        path -> {
                            try {
                                return Files.lines(path)
                                        .reduce((s1, s2) -> s1 + s2)
                                        .orElse("");
                            } catch (IOException e) {
                                e.printStackTrace();
                                throw new RuntimeException(e);
                            }
                        })
                );
    }

    private static Map<String, Set<String>> loadFilesLines(URL folder) throws IOException, URISyntaxException {
        return Files.list(Paths.get(folder.toURI()))
                .filter(Files::isReadable)
                .collect(Collectors.toMap(
                        path -> path.getFileName().toString(),
                        path -> {
                            try {
                                return Files.lines(path)
                                        .map(String::trim)
                                        .filter(StringUtils::isNotEmpty)
                                        .map(String::toLowerCase)
                                        .collect(Collectors.toSet());
                            } catch (IOException e) {
                                e.printStackTrace();
                                throw new RuntimeException(e);
                            }
                        })
                );
    }

    private static List<VocabularyWord> getVocabulary(Map<String, String> fileToText, NGramStrategy nGramStrategy) {
        List<String> vocabulary = fileToText.values().stream()
                .map(nGramStrategy::getNGram)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        List<VocabularyWord> vocabularyWords = new ArrayList<>();
        for (int i = 0; i < vocabulary.size(); i++) {
            vocabularyWords.add(new VocabularyWord(i, vocabulary.get(i)));
        }
        return vocabularyWords;
    }

    private static Set<CharacteristicValue> getCategories(Map<String, Set<String>> fileToCategories) {
        Set<CharacteristicValue> characteristicValues = new HashSet<>();
        int categoryId = 0;
        Set<String> categories = fileToCategories.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        for (String category : categories) {
            characteristicValues.add(new CharacteristicValue(categoryId++, category));
        }
        return characteristicValues;
    }

    private static List<ClassifiableText> getClassifiableTexts(Map<String, String> fileToText,
                                                               Map<String, Set<String>> fileToCategory,
                                                               Set<CharacteristicValue> characteristicValues,
                                                               Characteristic characteristic) {
        return fileToText.entrySet().stream()
                .map(text -> {
                    String fileName = text.getKey();
                    Set<String> textCategories = fileToCategory.get(fileName);
                    Map<String, CharacteristicValue> characteristicValueMap = characteristicValues.stream()
                            .collect(Collectors.toMap(CharacteristicValue::getValue, Function.identity()));

                    Set<CharacteristicValue> categories = textCategories.stream()
                            .map(characteristicValueMap::get)
                            .collect(Collectors.toSet());

                    return new ClassifiableText(
                            text.getValue(),
                            Collections.singletonMap(characteristic, categories)
                    );
                })
                .collect(Collectors.toList());
    }
}
