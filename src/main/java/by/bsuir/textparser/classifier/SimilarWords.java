package by.bsuir.textparser.classifier;

public class SimilarWords {
    
    private WordCharacteristic unknownWord;
    
    private WordCharacteristic knownWord;
    private double knownWordProbability;

    public WordCharacteristic getUnknownWord() {
        return unknownWord;
    }

    public void setUnknownWord(WordCharacteristic unknownWord) {
        this.unknownWord = unknownWord;
    }

    public WordCharacteristic getKnownWord() {
        return knownWord;
    }

    public void setKnownWord(WordCharacteristic knownWord) {
        this.knownWord = knownWord;
    }

    public double getKnownWordProbability() {
        return knownWordProbability;
    }

    public void setKnownWordProbability(double knownWordProbability) {
        this.knownWordProbability = knownWordProbability;
    }
}
