package by.bsuir.textparser.classifier.model;

public class CharacteristicValue {

    private int orderNumber;
    private String value;

    public CharacteristicValue(int orderNumber, String value) {
        this.orderNumber = orderNumber;
        this.value = value;
    }

    public CharacteristicValue(String value) {
        this(0, value);
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof CharacteristicValue) && (this.value.equals(((CharacteristicValue) o).getValue())));
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }
}