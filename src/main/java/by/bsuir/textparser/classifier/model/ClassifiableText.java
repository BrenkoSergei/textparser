package by.bsuir.textparser.classifier.model;

import java.util.Map;
import java.util.Set;

public class ClassifiableText {

    private String text;
    private Map<Characteristic, Set<CharacteristicValue>> characteristics;

    public ClassifiableText(String text, Map<Characteristic, Set<CharacteristicValue>> characteristics) {
        this.text = text;
        this.characteristics = characteristics;
    }

    public ClassifiableText(String text) {
        this(text, null);
    }

    public String getText() {
        return text;
    }

    public Map<Characteristic, Set<CharacteristicValue>> getCharacteristics() {
        return characteristics;
    }

    public Set<CharacteristicValue> getCharacteristicValue(String characteristicName) {
        return characteristics.get(new Characteristic(characteristicName));
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ClassifiableText) && this.text.equals(((ClassifiableText) o).getText()) && this.characteristics.equals(((ClassifiableText) o).getCharacteristics());
    }
}