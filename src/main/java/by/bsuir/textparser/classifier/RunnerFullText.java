package by.bsuir.textparser.classifier;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import by.bsuir.textparser.classifier.model.Characteristic;
import by.bsuir.textparser.classifier.model.CharacteristicValue;
import by.bsuir.textparser.classifier.model.ClassifiableText;
import by.bsuir.textparser.classifier.model.VocabularyWord;
import by.bsuir.textparser.classifier.ngram.FilteredUnigram;
import by.bsuir.textparser.classifier.ngram.NGramStrategy;
import org.apache.commons.lang3.StringUtils;

public class RunnerFullText {

    private static final String TEXTS_FOLDER = "text";
    private static final String CATEGORY_FOLDER = "category";

    private static final String REAL_DATA_FOLDER = "check";

    public static void main(String[] args) throws URISyntaxException, IOException {
        TextClassifier classifier = trainNetwork();

        URL resource = RunnerFullText.class.getClassLoader().getResource(REAL_DATA_FOLDER);
        Map<String, String> texts = loadFiles(resource);
        texts.forEach((file, text) -> {
            Map<String, Double> classification = classifier.classify(text);

            System.out.println("=======" + file + "=======");
            classification.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder()))
                    .limit(10)
                    .map(categoryToProbability -> String.format("%s -> %1.6f", categoryToProbability.getKey(), categoryToProbability.getValue()))
                    .forEach(System.out::println);
            System.out.println();
        });
    }

    private static TextClassifier trainNetwork() throws IOException, URISyntaxException {
        URL textsFolder = RunnerFullText.class.getClassLoader().getResource(TEXTS_FOLDER);
        URL categoriesFolder = RunnerFullText.class.getClassLoader().getResource(CATEGORY_FOLDER);

        Map<String, String> fileToText = loadFiles(textsFolder);
        Map<String, Set<String>> fileToCategory = loadFileLines(categoriesFolder);

        NGramStrategy nGramStrategy = new FilteredUnigram();
        List<VocabularyWord> vocabularyWords = getVocabulary(fileToText, nGramStrategy);
        Set<CharacteristicValue> characteristicValues = getCharacteristicValues(fileToCategory);

        Characteristic characteristic = new Characteristic("Category", characteristicValues);
        TextClassifier classifier = new TextClassifier(characteristic, vocabularyWords, nGramStrategy);

        List<ClassifiableText> classifiableTexts = getClassifiableTexts(fileToText, fileToCategory, characteristicValues, characteristic);

        classifier.train(classifiableTexts);
        return classifier;
    }

    private static Map<String, String> loadFiles(URL folder) throws IOException, URISyntaxException {
        return Files.list(Paths.get(folder.toURI()))
                .filter(Files::isReadable)
                .collect(Collectors.toMap(
                        path -> path.getFileName().toString(),
                        path -> {
                            try {
                                return Files.lines(path)
                                        .reduce((s, s2) -> s + s2)
                                        .orElse("");
                            } catch (IOException e) {
                                e.printStackTrace();
                                return "";
                            }
                        })
                );
    }

    private static Map<String, Set<String>> loadFileLines(URL folder) throws IOException, URISyntaxException {
        return Files.list(Paths.get(folder.toURI()))
                .filter(Files::isReadable)
                .collect(Collectors.toMap(
                        path -> path.getFileName().toString(),
                        path -> {
                            try {
                                return Files.lines(path)
                                        .map(String::trim)
                                        .filter(StringUtils::isNotEmpty)
                                        .map(String::toLowerCase)
                                        .collect(Collectors.toSet());
                            } catch (IOException e) {
                                e.printStackTrace();
                                return Collections.emptySet();
                            }
                        })
                );
    }

    private static List<VocabularyWord> getVocabulary(Map<String, String> fileToText, NGramStrategy nGramStrategy) {
        List<String> vocabulary = fileToText.values().stream()
                .map(nGramStrategy::getNGram)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        List<VocabularyWord> vocabularyWords = new ArrayList<>();
        for (int i = 0; i < vocabulary.size(); i++) {
            vocabularyWords.add(new VocabularyWord(i, vocabulary.get(i)));
        }
        return vocabularyWords;
    }

    private static Set<CharacteristicValue> getCharacteristicValues(Map<String, Set<String>> fileToCategories) {
        Set<CharacteristicValue> characteristicValues = new HashSet<>();
        int categoryId = 0;
        Set<String> categories = fileToCategories.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        for (String category : categories) {
            characteristicValues.add(new CharacteristicValue(categoryId++, category));
        }
        return characteristicValues;
    }

    private static List<ClassifiableText> getClassifiableTexts(Map<String, String> fileToText,
                                                               Map<String, Set<String>> fileToCategory,
                                                               Set<CharacteristicValue> characteristicValues,
                                                               Characteristic characteristic) {
        return fileToText.entrySet().stream()
                .map(text -> {
                    String fileName = text.getKey();
                    Set<String> textCategories = fileToCategory.get(fileName);
                    Map<String, CharacteristicValue> characteristicValueMap = characteristicValues.stream()
                            .collect(Collectors.toMap(CharacteristicValue::getValue, Function.identity()));

                    Set<CharacteristicValue> categories = textCategories.stream()
                            .map(characteristicValueMap::get)
                            .collect(Collectors.toSet());

                    return new ClassifiableText(
                            text.getValue(),
                            Collections.singletonMap(characteristic, categories)
                    );
                })
                .collect(Collectors.toList());
    }
}
