package by.bsuir.textparser.entity;

import by.bsuir.textparser.Word;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sentence {

    private int index;
    private List<Word> wordList = new ArrayList<>();
    private String originalSentence;
    private Text text;

    public Sentence(String originalSentence) {
        this.originalSentence = originalSentence;
    }

    public Sentence(String originalSentence, List<Word> wordList) {
        this.originalSentence = originalSentence;
        this.wordList = wordList;
    }


    public Sentence(List<Word> wordList, Text text) {
        this.wordList = wordList;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    public String getOriginalSentence() {
        return originalSentence;
    }

    public void setOriginalSentence(String originalSentence) {
        this.originalSentence = originalSentence;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return Arrays.toString(wordList.toArray());
    }
}
