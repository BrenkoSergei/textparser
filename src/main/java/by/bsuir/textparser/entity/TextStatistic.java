package by.bsuir.textparser.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import by.bsuir.textparser.Word;
import by.bsuir.textparser.utill.Pair;

public class TextStatistic {

    private static final int KEY_WORD_START_INDEX = 0;
    private static final int KEY_WORD_SIZE = 7;

    private Text text;
    private Integer wordCount;
    private Integer averageWordCountPerSentence;

    private Map<Word, List<Word>> wordMapping = new HashMap<>();
    private List<Pair<Word, Integer>> sortedWordList = new ArrayList<>();

    public TextStatistic(Text text) {
        this.text = text;
    }

    public List<Word> getWords(int start, int end) {
        return sortedWordList.subList(start, end).stream()
                .map(Pair::getFirstValue)
                .collect(Collectors.toList());
    }

    public List<Word> getKeyWords(int start, int count) {
        return getWords(start, start + count);
    }

    public List<Word> getKeyWords() {
        return getKeyWords(KEY_WORD_START_INDEX, KEY_WORD_SIZE);
    }

    public Map<Word, List<Word>> getWordMapping() {
        return wordMapping;
    }

    public void setWordMapping(Map<Word, List<Word>> wordMapping) {
        this.wordMapping = wordMapping;
    }

    public List<Pair<Word, Integer>> getSortedWordList() {
        return sortedWordList;
    }

    public void setSortedWordList(List<Pair<Word, Integer>> sortedWordList) {
        this.sortedWordList = sortedWordList;
    }


    public List<Word> getOriginalWordList(Word word) {
        return wordMapping.getOrDefault(word, Collections.emptyList());
    }

    public void printStatistic(int count) {
        for (Pair<Word, Integer> pair : sortedWordList) {
            System.out.println(pair.getFirstValue() + " = " + pair.getSecondValue());
            if (--count == 0) {
                break;
            }
        }
    }

    public int getWordCount() {
        if (wordCount == null) {
            wordCount = text.getSentenceList().stream().mapToInt(s -> s.getWordList().size()).sum();
        }
        return wordCount;
    }

    public int getAverageWordCountPerSentence() {
        return getWordCount() / text.getSentenceList().size();
    }
}
