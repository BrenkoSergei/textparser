package by.bsuir.textparser.entity;

import by.bsuir.textparser.Word;
import by.bsuir.textparser.analyzer.stopword.StopWordAnalyzer;
import by.bsuir.textparser.analyzer.word.WordAnalyzer;
import by.bsuir.textparser.utill.Pair;

import java.util.*;

public class TextStatisticCreator {

    public static TextStatistic create(Text text) {
        Map<Word, List<Word>> wordMapping = createWordMapping(text);
        List<Pair<Word, Integer>> sortedWordList = getSortedWordList(wordMapping);

        TextStatistic textStatistic = new TextStatistic(text);
        textStatistic.setWordMapping(wordMapping);
        textStatistic.setSortedWordList(sortedWordList);

        return textStatistic;
    }

    private static Map<Word, List<Word>> createWordMapping(Text text) {
        WordAnalyzer wordAnalyzer = text.getWordAnalyzer();
        StopWordAnalyzer swAnalyzer = text.getStopWordAnalyzer();

        Map<Word, List<Word>> wordMapping = new HashMap<>();

        for(Sentence sentence : text.getSentenceList()) {
            for(Word word : sentence.getWordList()) {
                if(swAnalyzer.isStopWord(word.getWord())) {
                    continue;
                }
                String baseWord = wordAnalyzer.getBaseForm(word.getWord());
                Word keyWord = new Word(baseWord);

                List<Word> wordList = wordMapping.get(keyWord);
                if(wordList == null) {
                    List<Word> synonymList = new ArrayList<>();
                    synonymList.add(word);
                    wordMapping.put(keyWord, synonymList);
                } else {
                    wordList.add(word);
                }
            }
        }

        return wordMapping;
    }


    private static List<Pair<Word, Integer>> getSortedWordList(Map<Word, List<Word>> wordMapping) {
        List<Pair<Word, Integer>> sortedWordList = new ArrayList<>(wordMapping.size());
        for(Map.Entry<Word, List<Word>> entry : wordMapping.entrySet()) {
            sortedWordList.add(new Pair<>(entry.getKey(), entry.getValue().size()));
        }
        sortedWordList.sort((a, b) -> -Integer.compare(a.getSecondValue(), b.getSecondValue()));
        return sortedWordList;
    }
}
