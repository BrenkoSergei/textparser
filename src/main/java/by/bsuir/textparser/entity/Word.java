package by.bsuir.textparser;

import by.bsuir.textparser.entity.Sentence;

public class Word {

    private int index;
    private String word;
    private Sentence sentence;

    /*public Word(String initialWord) {
        this.initialWord = initialWord;
    }*/

    public Word(String word) {
        this.word = word;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public void setSentence(Sentence sentence) {
        this.sentence = sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if(o == null || getClass() != o.getClass()) {
            return false;
        }

        Word obj = (Word) o;

        return word != null ? word.equals(obj.word) : obj.word == null;
    }

    @Override
    public int hashCode() {
        return word != null ? word.hashCode() : 0;
    }

    @Override
    public String toString() {
        return word;
    }
}
