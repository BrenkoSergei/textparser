package by.bsuir.textparser.entity;

import java.util.ArrayList;
import java.util.List;

public class TextGroup {

    private String name;
    private Text baseText;
    private List<Text> similarTextList;

    public TextGroup(Text baseText, String name) {
        this(baseText, name, new ArrayList<>());
    }

    public TextGroup(Text baseText, String name, List<Text> similarTextList) {
        this.baseText = baseText;
        this.name = name;
        this.similarTextList = similarTextList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Text getBaseText() {
        return baseText;
    }

    public void setBaseText(Text baseText) {
        this.baseText = baseText;
    }

    public List<Text> getSimilarTextList() {
        return similarTextList;
    }

    public void setSimilarTextList(List<Text> similarTextList) {
        this.similarTextList = similarTextList;
    }

    public void addSimilarText(Text text) {
        similarTextList.add(text);
    }
}
