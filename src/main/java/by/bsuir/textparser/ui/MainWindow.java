package by.bsuir.textparser.ui;

import by.bsuir.textparser.utill.FileUtil;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MainWindow extends JFrame {

    private JPanel panel;

    private JTextArea sourceTextArea;
    private JTextArea keywordTextArea;
    private JTextArea summaryTextArea;

    private JSlider percentSlider;
    private JLabel percentLabel;

    private JLabel sourceWordsLabel;
    private JLabel sourceCharsLabel;
    private JLabel sourceLinesLabel;
    private JLabel summaryWordsLabel;
    private JLabel summaryCharsLabel;
    private JLabel summaryLinesLabel;

    public MainWindow() {
        initUI();
    }



    public void initUI() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");

        JMenuItem openMenuItem = new JMenuItem("Open");
        openMenuItem.addActionListener(new OpenActionListener());

        fileMenu.add(openMenuItem);

        menuBar.add(fileMenu);

        sourceTextArea = new JTextArea();
        sourceTextArea.setLineWrap(true);
        sourceTextArea.setWrapStyleWord(true);
        sourceTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8));

        JScrollPane sourceScrollPane = new JScrollPane();
        sourceScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        sourceScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sourceScrollPane.getViewport().add(sourceTextArea);

        keywordTextArea = new JTextArea();
        keywordTextArea.setLineWrap(true);
        keywordTextArea.setWrapStyleWord(true);
        keywordTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8));

        JScrollPane keywordScrollPane = new JScrollPane();
        keywordScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        keywordScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        keywordScrollPane.getViewport().add(keywordTextArea);
        keywordScrollPane.setPreferredSize(new Dimension(keywordScrollPane.getMaximumSize().width, 30));

        summaryTextArea = new JTextArea();
        summaryTextArea.setLineWrap(true);
        summaryTextArea.setWrapStyleWord(true);
        summaryTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8,8 ));

        JScrollPane summaryScrollPane = new JScrollPane();
        summaryScrollPane.setAlignmentX(LEFT_ALIGNMENT);
        summaryScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        summaryScrollPane.getViewport().add(summaryTextArea);

        JLabel sourceTitleLabel = new JLabel("Source");
        sourceTitleLabel.setAlignmentX(LEFT_ALIGNMENT);
        JLabel keywordTitleLabel = new JLabel("Keyword");
        keywordTitleLabel.setAlignmentX(LEFT_ALIGNMENT);
        JLabel summaryTitleLabel = new JLabel("Summary");
        summaryTitleLabel.setAlignmentX(LEFT_ALIGNMENT);

        sourceCharsLabel = new JLabel("Characters: ");
        sourceCharsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sourceWordsLabel = new JLabel("Words: ");
        sourceWordsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sourceLinesLabel = new JLabel("Lines: ");
        sourceLinesLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        summaryCharsLabel = new JLabel("Characters: ");
        summaryCharsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        summaryWordsLabel = new JLabel("Words: ");
        summaryWordsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        summaryLinesLabel = new JLabel("Lines: ");
        summaryLinesLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
        leftPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 8));

        leftPanel.add(sourceTitleLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceScrollPane);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        leftPanel.add(sourceCharsLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceWordsLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        leftPanel.add(sourceLinesLabel);

        rightPanel.add(keywordTitleLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(keywordScrollPane);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPanel.add(summaryTitleLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryScrollPane);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPanel.add(summaryCharsLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryWordsLabel);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        rightPanel.add(summaryLinesLabel);

        JPanel centralPanel = new JPanel();
        centralPanel.setLayout(new GridLayout(1, 2));

        centralPanel.add(leftPanel, BorderLayout.WEST);
        centralPanel.add(rightPanel, BorderLayout.EAST);

        percentSlider = new JSlider();
        percentSlider.setBorder(BorderFactory.createTitledBorder("Summary Length"));
        percentSlider.setValue(20);
        percentSlider.setMajorTickSpacing(20);
        percentSlider.setMinorTickSpacing(5);
        percentSlider.setPaintTicks(true);
        percentSlider.addChangeListener(new SliderChangeListener());

        percentLabel = new JLabel("20%");

        JButton summarizeButton = new JButton("Summarize");
        summarizeButton.setBounds(50, 60, 80, 30);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));

        bottomPanel.add(percentSlider);
        bottomPanel.add(percentLabel);
        bottomPanel.add(Box.createHorizontalGlue());
        bottomPanel.add(summarizeButton);

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        panel.add(centralPanel, BorderLayout.CENTER);
        panel.add(bottomPanel, BorderLayout.SOUTH);

        add(panel);
        setJMenuBar(menuBar);
        setTitle("Text");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private class OpenActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
            fileChooser.setFileFilter(filter);

            int retVal = fileChooser.showDialog(panel, "Open File");
            if(retVal == JFileChooser.APPROVE_OPTION) {
//                File file = fileChooser.getSelectedFile();
//                String text = FileUtil.loadTextFromFile(file);
//                sourceTextArea.setText(text);
            }
        }
    }

    private class SliderChangeListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider slider = (JSlider) e.getSource();
            int val = slider.getValue();
            percentLabel.setText(val + "%");
        }
    }
}

