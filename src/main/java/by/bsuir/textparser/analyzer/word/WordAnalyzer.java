package by.bsuir.textparser.analyzer.word;

public interface WordAnalyzer {

    default String getBaseForm(String word) {
        return word.toLowerCase();
    }
}
