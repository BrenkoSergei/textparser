package by.bsuir.textparser.analyzer.text;

import by.bsuir.textparser.entity.Text;

public interface TextAnalyzer {

    boolean isSimilar(Text text1, Text text2);
}
