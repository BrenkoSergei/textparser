package by.bsuir.textparser.analyzer.stopword;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import by.bsuir.textparser.utill.FileUtil;

public class EnglishStopWordAnalyzer implements StopWordAnalyzer {

    private static Set<String> ENGLISH_STOP_WORDS;

    static {
        URL resource = EnglishStopWordAnalyzer.class.getClassLoader().getResource("stopWords/eng.txt");
        ENGLISH_STOP_WORDS = new HashSet<>(FileUtil.loadWordsFromFile(resource));
    }

    @Override
    public boolean isStopWord(String word) {
        return ENGLISH_STOP_WORDS.contains(word.toLowerCase());
    }
}
