package by.bsuir.textparser.utill;

public class Pair<K, E> {

    private K firstValue;
    private E secondValue;

    public Pair(){
    }

    public Pair(K firstValue, E secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    public K getFirstValue() {
        return firstValue;
    }

    public void setFirstValue(K firstValue) {
        this.firstValue = firstValue;
    }

    public E getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(E secondValue) {
        this.secondValue = secondValue;
    }
}
