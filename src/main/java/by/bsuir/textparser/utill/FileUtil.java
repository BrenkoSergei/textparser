package by.bsuir.textparser.utill;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class FileUtil {

    public static List<String> loadWordsFromFile(URL file) {
        try {
            return Files.lines(Paths.get(file.toURI()))
                    .map(String::trim)
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static String loadTextFromFile(Path file) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String lineSeparator = System.getProperty("line.separator");

            Files.lines(file)
                    .filter(StringUtils::isNotEmpty)
                    .forEach(line -> stringBuilder.append(line).append(lineSeparator));
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static List<File> loadFilesFromDirectory(File folder) {
        List<File> fileList = new ArrayList<>();
        for (File file : folder.listFiles()) {
            if (file.isFile()) {
                fileList.add(file);
            } else {
                fileList.addAll(loadFilesFromDirectory(file));
            }
        }
        return fileList;
    }

    public static void writeStringToFile(List<String> stringList, File file) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (String str : stringList) {
                bw.write(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
