package by.bsuir.textparser.utill;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import by.bsuir.textparser.Word;
import by.bsuir.textparser.entity.Sentence;
import by.bsuir.textparser.entity.Text;

public class TextParser {

    private static final Pattern SENTENCE_PATTERN = Pattern.compile("[.?!]+");
    //private static final Pattern WORD_PATTERN = Pattern.compile("[^А-Яа-яёЁ]+");
//    private static final Pattern WORD_PATTERN = Pattern.compile("[^A-Za-z]+");
    private static final Pattern WORD_PATTERN = Pattern.compile("[^A-Za-zА-Яа-яёЁ]+");

    public static Text buildText(String textName, String stringInput) {
        Text text = new Text(textName);
        text.setSentenceList(parseText(stringInput, text));
        return text;
    }

    private static List<Sentence> parseText(String textStr, Text text) {
        List<Sentence> sentenceList = new ArrayList<>();
        String[] sentences = SENTENCE_PATTERN.split(textStr);

        for (int i = 0; i < sentences.length; i++) {
            Sentence sentence = new Sentence(sentences[i]);
            List<Word> wordList = parseSentence(sentences[i], sentence);
            if (!wordList.isEmpty()) {
                sentence.setIndex(i);
                sentence.setText(text);
                sentence.setWordList(wordList);
                sentenceList.add(sentence);
            }
        }
        return sentenceList;
    }

    private static List<Word> parseSentence(String sentenceStr, Sentence sentence) {
        List<Word> wordList = new ArrayList<>();
        String[] words = WORD_PATTERN.split(sentenceStr);

        for (int i = 0; i < words.length; i++) {
            String trimmedWord = words[i].trim();
            if (trimmedWord.length() > 1) {
                Word word = new Word(trimmedWord);
                word.setIndex(i);
                word.setSentence(sentence);
                wordList.add(word);
            }
        }
        return wordList;
    }
}
