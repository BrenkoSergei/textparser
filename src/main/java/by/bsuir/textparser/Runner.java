package by.bsuir.textparser;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import by.bsuir.textparser.entity.Text;
import by.bsuir.textparser.entity.TextStatistic;
import by.bsuir.textparser.utill.FileUtil;
import by.bsuir.textparser.utill.TextParser;

public class Runner {

    public static void main(String[] args) throws URISyntaxException {






        long start = System.currentTimeMillis();
        URL resource = Runner.class.getClassLoader().getResource("text/1.txt");

        String str = FileUtil.loadTextFromFile(Paths.get(resource.toURI()));
        System.out.println("Text load time: " + (System.currentTimeMillis() - start) + " ms");

        Text text = TextParser.buildText("1.txt", str);
        TextStatistic textStatistic = text.getTextStatistic();

//        System.out.println("======= Sentences =======");
//        text.getSentenceList().stream()
//                .limit(100)
//                .forEach(System.out::println);
//        System.out.println();

//        textStatistic.printStatistic(100);

        System.out.println("------------ Key words ------------");
        textStatistic.getKeyWords().stream()
                .map(by.bsuir.textparser.Word::getWord)
                .forEach(System.out::println);

        System.out.println("Words: " + str.length());

        /*List<File> fileList = new ArrayList<>();
        fileList.add(new File("/home/mint/work/java/textparser/src/main/resources/matrix.txt"));
        List<Text> textList = fileList.stream().map(RunnerKeyWords::buildTextFormFile).collect(Collectors.toList());
        TextService ts = new TextServiceImpl();
        List<String> summary = ts.summarize(textList.get(0), 0.2f);
        FileUtil.writeStringToFile(summary, new File("/home/mint/work/java/textparser/src/main/resources/1.txt"));*/
        /*List<TextGroup> similarText = ts.findSimilarText(textList, 2);

        for(Text text : textList) {
            System.out.println(text.getName() + " = " + Arrays.toString(text.getTextStatistic().getKeyWords().toArray()));
        }

        for(Text text : textList) {
            System.out.println(text.getName() + " = ==================");
            text.getTextStatistic().printStatistic(10);
            System.out.println(text.getName() + " = ==================");
        }

        similarText.forEach(textGroup -> System.out.println(textGroup.getName() + " = " + Arrays.toString(textGroup.getSimilarTextList().toArray())));*/

//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                MainWindow mainWindow = new MainWindow();
//                mainWindow.setVisible(true);
//            }
//        });
    }

//    private static Text buildTextFormFile(File file) {
//        String str = FileUtil.loadTextFromFile(file);
//        return TextParser.buildText(file.getName(), str);
//    }
}
